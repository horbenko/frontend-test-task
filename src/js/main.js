'use strict';

let countCommentsLoaded = 0;

// Demo data
var response = [
    {
        "id": 7876,
        "content": "If not everyone makes money blogging, why is blogging so popular?",
        "created_at": "2019-08-10T06:59:11.326Z",
        "updated_at": "2019-08-10T06:59:11.326Z",
        "author": {
            "id": 1,
            "name": "Kurt Thompson",
            "avatar": "http://api.randomuser.me/portraits/thumb/men/69.jpg",
            "created_at": "2015-08-11T13:08:25.675Z",
            "updated_at": "2015-08-11T13:08:25.675Z"
        },
        "children": [
            {
                "id": 7877,
                "content": "A WordPress blog will stand out!",
                "created_at": "2019-08-10T06:59:11.334Z",
                "updated_at": "2019-08-10T06:59:11.334Z",
                "author": {
                    "id": 3,
                    "name": "Sarah Fleming",
                    "avatar": "http://api.randomuser.me/portraits/thumb/women/80.jpg",
                    "created_at": "2015-08-11T13:08:25.687Z",
                    "updated_at": "2015-08-11T13:08:25.687Z"
                }
            },
            {
                "id": 7878,
                "content": "The possibilities of designing a site with WordPress are immense indeed.",
                "created_at": "2019-08-10T06:59:11.343Z",
                "updated_at": "2019-08-10T06:59:11.343Z",
                "author": {
                    "id": 4,
                    "name": "Purificacion Rojas",
                    "avatar": "http://api.randomuser.me/portraits/thumb/women/2.jpg",
                    "created_at": "2015-08-11T13:08:25.694Z",
                    "updated_at": "2015-08-11T13:08:25.694Z"
                }
            },
            {
                "id": 7879,
                "content": "Anyone can blog but it takes a WordPress user to be awesome!",
                "created_at": "2019-08-10T06:59:11.349Z",
                "updated_at": "2019-08-10T06:59:11.349Z",
                "author": {
                    "id": 5,
                    "name": "Phillip Lynch",
                    "avatar": "http://api.randomuser.me/portraits/thumb/men/68.jpg",
                    "created_at": "2015-08-11T13:08:25.702Z",
                    "updated_at": "2015-08-11T13:08:25.702Z"
                }
            }
        ]
    },
    {
        "id": 7874,
        "content": "Each day I love you more my blog Today more than yesterday and less than tomorrow",
        "created_at": "2019-08-10T06:59:11.309Z",
        "updated_at": "2019-08-10T06:59:11.309Z",
        "author": {
            "id": 1,
            "name": "Kurt Thompson",
            "avatar": "http://api.randomuser.me/portraits/thumb/men/69.jpg",
            "created_at": "2015-08-11T13:08:25.675Z",
            "updated_at": "2015-08-11T13:08:25.675Z"
        },
        "children": [
            {
                "id": 7875,
                "content": "I claim there ain’t another software for blogging As great as WordPress. :)",
                "created_at": "2019-08-10T06:59:11.317Z",
                "updated_at": "2019-08-10T06:59:11.317Z",
                "author": {
                    "id": 5,
                    "name": "Phillip Lynch",
                    "avatar": "http://api.randomuser.me/portraits/thumb/men/68.jpg",
                    "created_at": "2015-08-11T13:08:25.702Z",
                    "updated_at": "2015-08-11T13:08:25.702Z"
                }
            }
        ]
    },
    {
        "id": 7873,
        "content": "WordPress is not responsible for people falling in love with blogging!",
        "created_at": "2019-08-10T06:59:11.303Z",
        "updated_at": "2019-08-10T06:59:11.303Z",
        "author": {
            "id": 4,
            "name": "Purificacion Rojas",
            "avatar": "http://api.randomuser.me/portraits/thumb/women/2.jpg",
            "created_at": "2015-08-11T13:08:25.694Z",
            "updated_at": "2015-08-11T13:08:25.694Z"
        },
        "children": []
    },
    {
        "id": 7872,
        "content": "It is hard to defeat WordPress as a blogging tool. :)",
        "created_at": "2019-08-10T06:59:11.290Z",
        "updated_at": "2019-08-10T06:59:11.290Z",
        "author": {
            "id": 5,
            "name": "Phillip Lynch",
            "avatar": "http://api.randomuser.me/portraits/thumb/men/68.jpg",
            "created_at": "2015-08-11T13:08:25.702Z",
            "updated_at": "2015-08-11T13:08:25.702Z"
        },
        "children": []
    }
];



jQuery(function($){

  /*
   * 1. Listing of last 5 comments (check API description below)
  */
  getComments(5, 0)


  /*
   * 2. Load more (+5) comments
  */
  loadMoreComments.onclick = function(){
    getComments(5, countCommentsLoaded)
  }


  /*
   * 3. Add new comment
  */
  addNewComment.onsubmit = function(){
    if(!this.childNodes[1].value.length)
      return false

    let messageValue = this.childNodes[1].value
    this.childNodes[1].value = ''

    $.ajax({
      url: 'http://frontend-test.pingbull.com/pages/artemhorbenko@gmail.com/comments/',
      type: 'POST',
      dateType: 'json',
      data: {
        'content': messageValue
      }
    })
    .done(function(data) {
      // console.log(data)
      let comment = createCommentNode(data)
      $('#lastComments').prepend(comment)
      countCommentsLoaded++
    });
    return false
  }






  /*
   * 4. Reply on comment
  */

  // Open form reply to comment 
  $('#lastComments').on('click', '.replyToCommentJS', function(){
    openReplyForm(this)
  });
  // Close form reply to comment 
  $('#lastComments').on('click', '.closeReplyToCommentJS', function(){
    let wrap = $(this).parents('.reply-to-comment')
    wrap.slideUp('slow')
    setTimeout(function(){
      wrap.remove();
    }, 1000);
  });
  $('#lastComments').on('submit', '.formreplyCommentJS', function(){

    let message = this.childNodes[1].value
    let id = $(this).parents('.comment-content').data().id
    let name = $(this).parents('.comment-content').find('.comment-title').html()

    let self = this

    $.ajax({
      url: 'http://frontend-test.pingbull.com/pages/artemhorbenko@gmail.com/comments/',
      type: 'POST',
      dateType: 'json',
      data: {
          _method: 'POST',
          'content': message,
          'parent': id
      }
    })
    .done(function(data) {
      console.log("success");
      // console.log(data);

      let comment = createChildCommentNode(data, name)
      if( $('#comment-'+data.parent).find('.replied-comments').length ){
        $('#comment-'+data.parent).find('.replied-comments').prepend(comment)
      }else{
        let wrap = $('<div>').addClass('replied-comments')
        comment.appendTo(wrap)
        $('#comment-'+data.parent).append(wrap)
      }

      // console.log(id)

      // let comment = createChildCommentNode(data, name)
      // console.log($(self));
      // console.log($(self).html());
      // $(self).parents('.comment').find.prepend(comment)
    })
    .fail(function() {
      console.log("error");
    })
    
    // $(this).parents('.comment-content').find('.comment-message').html(message)
    // TODO add new message to Front-End



    let wrap = $(this).parents('.reply-to-comment')
    wrap.hide()
    wrap.remove()

    return false
  })
  

  // Open reply form
  function openReplyForm(el){

    if( $(el).parents('.comment-content').children('.reply-to-comment').length )
      return
    if( $(el).parents('.comment-content').children('.edit-comment').length ){
      $(el).parents('.comment-content').children('.edit-comment').hide()
      $(el).parents('.comment-content').children('.edit-comment').remove()
    }

    let id = $(el).parents('.comment-content').data().id
    let name = $(el).parents('.comment-content').find('.comment-title').html()


    let html = `
      <div class="reply-to-comment">
        <div class="reply-to-comment__header">
          <div class="reply-to-comment__title"><i class="fas fa-reply fa-flip-horizontal"></i>  ${name}</div>
          <div class="reply-to-comment__cancel closeReplyToCommentJS"><i class="fas fa-times"></i>  Cancel</div>
        </div>
        <form class="reply-to-comment__form formreplyCommentJS">
          <textarea class="reply-to-comment__textarea" placeholder="Your Message"></textarea>
          <button class="reply-to-comment__button">Send</button>
        </form>
      </div>
    `;

    $(el).parents('.comment-content').append(html)

  }


  /*
   * 5. Edit own comments
  */
  $('#lastComments').on('click', '.commentEditJS', function(){
    openFormEditComment(this)
  })
  $('#lastComments').on('click', '.closeEditCommentJS', function(){
    let wrap = $(this).parents('.edit-comment')
    wrap.slideUp('slow')
    setTimeout(function(){
      wrap.remove();
    }, 1000);
  });
  $('#lastComments').on('submit', '.formEditCommentJS', function(){
    // console.log(this );
    // console.log( this.childNodes[1].value )
    // console.log( $(this).parents('.comment-content').data().id )

    let message = this.childNodes[1].value
    let id = $(this).parents('.comment-content').data().id

    $.ajax({
      url: 'http://frontend-test.pingbull.com/pages/artemhorbenko@gmail.com/comments/' + id,
      type: 'PUT',
      dateType: 'json',
      data: {
          _method: 'PUT',
          'content': message
      }
    })
    .done(function(data) {
      console.log("success");
      // console.log(data);
    })
    .fail(function() {
      console.log("error");
    })
    // .always(function() {
    //   console.log("complete");
    // });
    
    $(this).parents('.comment-content').find('.comment-message').html(message)

    let wrap = $(this).parents('.edit-comment')
    wrap.hide()
    wrap.remove()

    return false
  })

  function openFormEditComment(el){

    if( $(el).parents('.comment-content').children('.edit-comment').length )
      return
    if( $(el).parents('.comment-content').children('.reply-to-comment').length ){
      $(el).parents('.comment-content').children('.reply-to-comment').hide()
      $(el).parents('.comment-content').children('.reply-to-comment').remove()
    }

    // console.log( $(el).parents('.comment-content') )
    // console.log( $(el).parents('.comment-content').find('.comment-message') )
    // console.log( $(el).parents('.comment-content').find('.comment-message').html() )
    let message = $(el).parents('.comment-content').find('.comment-message').html();

    let html = `
      <div class="edit-comment__header">
        <div class="edit-comment__title"><i class="fas fa-edit fa-flip-horizontal"></i>  Edit comment</div>
        <div class="edit-comment__cancel closeEditCommentJS"><i class="fas fa-times"></i>  Cancel</div>
      </div>
      <form class="edit-comment__form formEditCommentJS">
        <textarea class="edit-comment__textarea">${message}</textarea>
        <button class="edit-comment__button">Send</button>
      </form>
    `;
    let formWrap = $('<div>').addClass('edit-comment').html(html)
    $(el).parents('.comment-content').append(formWrap)

  }






  /*
   * 6. Remove own comments
  */
  $('#lastComments').on('click', '.commentDeleteJS', function(){
    deleteComment( $(this).parents('.comment-content').data().id )
  })

  function deleteComment(id){
    $.ajax({
        url: 'http://frontend-test.pingbull.com/pages/artemhorbenko@gmail.com/comments/' + id,
        type: 'POST',
        dateType: 'json',
        data: {
            _method: 'DELETE'
        }
    })
    .done(function(data) {
      // console.log(data)
      countCommentsLoaded--
      $('#comment-'+id).hide('slow');
      setTimeout(function(){
        $('#comment-'+id).remove();
      }, 1000);
    })
  }

});









// Functions

function getComments(count = 5, offset = 0){
  $.ajax({
    url: 'http://frontend-test.pingbull.com/pages/artemhorbenko@gmail.com/comments/',
    type: 'GET',
    dateType: 'json',
    data: {
      'count': count,
      'offset': offset
    }
  })
  .done(function(data) {
    // console.log(data)
    if(data.length){
      countCommentsLoaded += 5
      for(let c of data){
        newCommentNode(c)
      }
    }else{
      $('#loadMoreComments').slideUp('fast');
    }
  });
}

function newCommentNode(commentData){

  /*  Comment item  */
  let comment = createCommentNode(commentData)

  /*  Replied comments  */
  if(commentData["children"].length){
      let repliedComments = $('<div>').addClass('replied-comments')
      for(let c of commentData["children"]){

        let childComment = createChildCommentNode(c, commentData.author.name)
        // let html = `
        //     <div class="comment-ava">
        //       <img src='${c.author.avatar}'>
        //     </div>
        //     <div class="comment-content">
        //       <div class="comment-header">
        //         <span class="comment-title">${c.author.name}</span>
        //         <span class="comment-replied"><i class="fas fa-reply fa-flip-horizontal"></i>  ${commentData.author.name}</span>
        //         <span class="comment-date"><i class="far fa-clock fa-flip-horizontal"></i>  ${datePostFormat(c.created_at)}</span>
        //       </div>
        //       <div class="comment-message">${c.content}</div>
        //     </div>
        //   `;
        // let comment = $('<div>').addClass('comment').html(html)
        // comment.appendTo(repliedComments)
        childComment.appendTo(repliedComments)
      }
      repliedComments.appendTo(comment)
  }
  $('#lastComments').append(comment)
}

function createCommentNode(commentData){
  let htmlActions;
  if(commentData.author.id == 1){
    htmlActions = `
    <div class="comment-actions">
      <span class="comment-actions__edit commentEditJS"><i class="far fa-edit"></i>  Edit     </span>
      <span class="comment-actions__delete commentDeleteJS"><i class="fas fa-times"></i>  Delete</span>     
      <span class="comment-actions__reply replyToCommentJS"><i class="fas fa-reply fa-flip-horizontal"></i>  Reply</span>
    </div>`;
  }else{
    htmlActions = '<div class="comment-actions"><i class="fas fa-reply fa-flip-horizontal"></i>  Reply</div>';
  }
  let html = `
    <div class="comment-ava">
      <img src='${commentData.author.avatar}'>
    </div>
    <div class="comment-content" data-id="${commentData.id}">
      <div class="comment-header">
        <span class="comment-title">${commentData.author.name}</span>
        <span class="comment-date"><i class="far fa-clock fa-flip-horizontal"></i>  ${datePostFormat(commentData.created_at)}</span>
      </div>
      <div class="comment-message">${commentData.content}</div>
      ${htmlActions}
    </div>
  `;
  return $('<div>').addClass('comment').attr('id','comment-' + commentData.id).html(html)
}

function createChildCommentNode(commentData, name){
  let html = `
      <div class="comment-ava">
        <img src='${commentData.author.avatar}'>
      </div>
      <div class="comment-content">
        <div class="comment-header">
          <span class="comment-title">${commentData.author.name}</span>
          <span class="comment-replied"><i class="fas fa-reply fa-flip-horizontal"></i>  ${name}</span>
          <span class="comment-date"><i class="far fa-clock fa-flip-horizontal"></i>  ${datePostFormat(commentData.created_at)}</span>
        </div>
        <div class="comment-message">${commentData.content}</div>
      </div>
    `;
  let comment = $('<div>').addClass('comment').html(html)
  return comment
}






// Helpers
function datePostFormat(dateString){
  let date = new Date(dateString),
      day = date.getDate(),
      month = date.getMonth() + 1,
      year = date.getUTCFullYear(),
      hours = date.getUTCHours(),
      minutes = date.getUTCMinutes();
  if (day < 10) day = "0" + day;
  if (month < 10) month = "0" + month;
  return year +'-'+month+'-'+day + ' at  '+hours+':'+minutes;
}

