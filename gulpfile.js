'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');


var plumber = require('gulp-plumber');
var webserver = require('gulp-webserver');
var livereload = require('gulp-livereload');
var fs = require('fs');


const babel = require('gulp-babel');

const dirs = {
  src: 'src',
  dest: './'
};

const sassPaths = {
  src: `${dirs.src}/sass/**/*.sass`,
  dest: `${dirs.dest}/styles/`
};

gulp.task('styles', () => {
  return gulp.src([sassPaths.src])
    .pipe(sourcemaps.init())
    // .pipe(sass.sync().on('error', plugins.sass.logError))
    .pipe(sass())
    .pipe(autoprefixer({cascade: false}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(sassPaths.dest));
});


gulp.task('scripts', () =>
    gulp.src('src/js/main.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('js'))
);


gulp.task('watch', ['server'], function() {
    gulp.watch(['src/sass/**/*.sass'], ['styles']);
    gulp.watch(['src/js/**/*.js'], ['scripts']);
    gulp.watch(['js/*.js'], livereload.reload);
    gulp.watch('styles/**/*.css', livereload.reload);
    gulp.watch('./*.html', livereload.reload);
});
gulp.task('server', function() {
    gulp.src('./')
        .pipe(plumber())
        .pipe(webserver({
            livereload: true,
            directoryListing: {
                enable: false,
                path: './'
            },
            open: true
        }));
});
